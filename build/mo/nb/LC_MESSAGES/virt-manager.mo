��    1      �  C   ,      8     9     N     Z     s     �     �  	   �     �  	   �     �     �     �     �     �     �     �     �     �                    "     ?     K     S     a     e     m     �     �  #   �     �     �     �     �  
   �                         %  	   +     5     <     A     R     X     `  �  s     $     B     P     k     x     ~     �  
   �  	   �     �     �     �     �     �     �  	   �     �     �     �     	     	     	  
   4	     ?	     F	     U	     [	  #   c	     �	     �	  $   �	     �	     �	     �	     �	     �	  	   
     
     
  	   
     '
  	   .
     8
     ?
     F
     U
     Z
  "   c
        	   )       '   
                                 (         &                       .             $            1   "       +   *       #                 -               !      /       %                   ,            0                <b>Basic details</b> <b>CPUs</b> <b>Example:</b> network1 <b>Memory</b> Active Architecture: CPU usage Co_nnect Completed Crashed Details Hypervisor: MAC address: Name Name: Network: Never Operation in progress Overview Pause Paused Please wait a few moments... Preferences Private Processing... Run Running Save Virtual Machine Screenshot Saving Virtual Machine Status: The network must be an IPv4 address Type: UUID: Virtual _Machine Virtual network _Browse... _Details _Edit _File _Finish _Help _Network: _Pause _Run _Take Screenshot _View seconds translator-credits Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-02-05 04:12+0000
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Norwegian Bokmål (http://www.transifex.com/projects/p/virt-manager/language/nb/)
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Zanata 4.6.2
 <b>Grunnleggende detaljer</b> <b>CPU-er</b> <b>Eksempel:</b> nettverk1 <b>Minne</b> Aktiv Arkitektur: CPU-bruk Ko_ble til Fullført Krasjet Detaljer Hypervisor: MAC-adresse: Navn Navn: Nettverk: Aldri Operasjon pågår Oversikt Pause Pauset Vennligst vent et øyeblikk... Brukervalg Privat Prosesserer... Kjør Kjører Lagre skjermdump av virtuell maskin Lagrer virtuell maskin Status: Nettverket må være en IPv4-adresse Type: UUID: Virtuell _maskin Virtuelt nettverk _Bla gjennom _Detaljer _Endre _Fil _Fullfør _Hjelp _Nettverk _Pause Kjø_r _Ta skjermdump _Vis sekunder Kjartan Maraas <kmaraas@gnome.org> 