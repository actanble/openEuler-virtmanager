��    P      �  k         �     �     �     �       +        =      Q     r     y  	   �     �  	   �     �     �     �     �     �     �     �               %  	   =     G     Z     g     l     r     z     �     �     �     �     �     �     �     �     �     	     	     !	     /	     8	     R	     V	     ^	     ~	  	   �	     �	     �	     �	     �	  ,   �	  /   �	  .    
  1   O
  +   �
  #   �
     �
     �
      �
     �
       *   *     U     f  
   v     �     �     �     �     �  	   �     �     �     �     �     �     �  �  �  "   �     �     �     �  C     "   F  9   i     �     �  &   �     �            >     7   ^     �     �  	   �     �     �  "   �  .        C  8   a     �     �     �  
   �     �     �     �  ,   �     #  
   9     D  ;   R     �     �     �     �     �     �  E     
   X     c  Y   p  ;   �               (     A     O  I   ]  9   �  K   �  ;   -  F   i  9   �     �     �  E   �  3   >  5   r  a   �      
     +     I     X     h  	   z  	   �     �     �     �     �     �     �     �  ,   �     ;   @       H                               I   4   #   8   =   A   $      E   3   K       !   D      <             -   1   /   M   N   L   7       '                   %   *   J           "      B          (   +      >          C   O          F   2             )   G          &             
   5                 0   ,   .                    	              ?   9      :       6             P       <b>Basic details</b> <b>CPUs</b> <b>Example:</b> network1 <b>Memory</b> <b>The console is currently unavailable</b> <b>Virtual Disk</b> <b>Virtual Network Interface</b> Active Architecture: CPU usage Co_nnect Completed Crashed Create a new virtual network Creating Virtual Machine Details Device: Gateway: Hypervisor: Inactive Invalid DHCP Address Invalid Network Address Location: Logical host CPUs: MAC address: Name Name: Network Network _Name: Network: Never Operation in progress Overview Pause Paused Physical Device Required Please wait a few moments... Powered by libvirt Preferences Private Processing... Reserved Restoring Virtual Machine Run Running Save Virtual Machine Screenshot Saving Virtual Machine Shut down Shutoff Source path: State: Status: The DHCP end address could not be understood The DHCP end address is not with the network %s The DHCP start address could not be understood The DHCP start address is not with the network %s The network address could not be understood The network must be an IPv4 address Type: UUID: Unable to complete install: '%s' Usermode networking Virtual Machine Manager Virtual Machine Manager Connection Failure Virtual _Machine Virtual network _Browse... _Details _Edit _File _Finish _Help _Network: _Pause _Run _Take Screenshot _View seconds translator-credits Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-02-05 04:13+0000
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Bulgarian (http://www.transifex.com/projects/p/virt-manager/language/bg/)
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Zanata 4.6.2
 <b>Базови детайли</b> <b>Процесори</b> <b>Пример:</b> network1 <b>Памет</b> <b>Конзолата в момента е недостъпна</b> <b>Виртуален диск</b> <b>Виртуален мрежов интерфейс</b> Активна Архитектура: Употреба на процесор _Свързване Готово Забила Създаване на нова виртуална мрежа Създаване на виртуална машина Детайли Устройство: Шлюз: _Хипервайзор: Неактивна Невалиден DHCP адрес Невалидни мрежови адреси Местоположение: Процесори на локалната машина: MAC адрес: Име Име: Мрежа _Име на мрежа: Мрежа: Никога Операцията се изпълнява Общ преглед Пауза В пауза Изисква се физическо устройство Моля изчакайте... Изградено с libvirt Настройки Частно Работи... Резервирано Възстановяване на виртуалната машина Старт Работи Запис на снимка на екранна на виртуалната машина Записване на виртуалната машина Изключване Спряна Път източник: Статус: Статус: Крайния DHCP адрес не може да бъде разбран Крайния DHCP адрес не е от мрежа %s Началния DHCP адрес не може да бъде разбран Началния DHCP адрес не е от мрежа %s Не може да бъде разбран мрежовия адрес Мрежата трябва да бъде IPv4 адрес Тип: UUID: Не може да се завърши инсталацията: '%s' Мрежа в потребителски режим Мениджър на виртуални машини Неуспех при връзка към мениджъра на виртуални машини Виртуална _Машина Виртуална мрежа _Избор... _Детайли _Редакция _Файл _Край _Помощ _Мрежа: _Пауза _Старт _Снимане _Преглед секунди Doncho N. Gunchev <gunchev@gmail.com>, 2007. 