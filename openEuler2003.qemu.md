
## openEuler20.03 安装 qemu-6.0 

```bash



yum -y install python3 python3-devel python3-pip 
pip3 install --upgrade pip 
pip3 install ipaddr ninja meson flake8 sphinx -U

yum install SDL SDL-devel
yum install gzlib-devel glib2-devel pixman-devel lzo-devel make gcc zlib-* -y


yum -y install bzip2-devel gtk3-devel gnutls-devel libpcap-devel libcap-ng-devel \
    libcurl-devel libaio-devel libxml2-devel spice-server-devel spice-protocol usbredir-devel \
    libjpeg-devel cyrus-sasl cyrus-sasl-devel cyrus-sasl-lib jemalloc-devel libusb-devel snappy-devel


git clone https://mirrors.tuna.tsinghua.edu.cn/git/qemu.git 
#git branch -a 
git checkout master  

# --target-list=i386-softmmu,x86_64-softmmu,i386-linux-user,x86_64-linux-user 
./configure --prefix=/usr --libexecdir=/usr/lib/qemu \
	--localstatedir=/var --bindir=/usr/bin/ --enable-gnutls \
	--enable-docs --enable-gtk --enable-vnc --enable-vnc-sasl \
	--enable-vnc-png --enable-vnc-jpeg --enable-curl --enable-kvm  \
	--enable-linux-aio --enable-cap-ng --enable-vhost-net \
	--enable-vhost-crypto --enable-spice --enable-usb-redir \
	--enable-lzo --enable-snappy --enable-bzip2 \
	--enable-coroutine-pool --enable-libxml2 \
	--enable-jemalloc --enable-replication \
	--enable-tools --enable-capstone
	
	
yum -y install git wget libguestfs-tools libvirt libosinfo -y


yum install libvirt-devel libvirt-daemon-kvm libvirt-client -y
pip3 install libvirt-python
cd virt-manger-2.1 && python3 setup.py install  

yum -y install libgcrypt xauth xclock virt-viewer

```

## 创建虚拟机示例

```bash



virt-install     \
    -n win11 \
    -r 4096 \
    --vcpus sockets=1,cores=2,threads=4  \
    --cdrom=/home/kvm/win11_en-us.iso \
    --disk /home/kvm/win11-base.qcow2,bus=virtio,format=qcow2,size=60 \
    --network bridge=virbr0,model=virtio \
    --os-type windows \
    --disk /data/vms/virtio-win-0.1.190_amd64.vfd,device=floppy  \
    --os-variant win10 \
    --noautoconsole \
    --graphics vnc,listen=0.0.0.0,port=6021  \
    --cpu host-passthrough \
    --boot hd
	


## KVM使用高阶教程

```bash
virt-install \
    --name xp \
    --memory 2048 \
    --vcpus sockets=1,cores=1,threads=2 \
    --cdrom=/data/iso/winxp-sp3.iso \
    --os-variant=windows \
    --disk /data/drivers/virtio-win-0.1.101_x86.vfd,device=floppy \
    --disk /data/vms/winxp-sp3.qcow2,bus=virtio,size=100 \
    --network bridge=virbr0,model=virtio \
    --noautoconsole \
    --accelerate \
    --hvm --virt-type kvm\
    --graphics vnc,listen=0.0.0.0,port=5903 \
    --cpu host-passthrough \
    --force --autostart


virt-install \
    --name win7 \
    --memory 2048 \
    --vcpus sockets=1,cores=1,threads=2 \
    --disk /data/vms/win7.qcow2,bus=virtio,format=qcow2,size=100 \
    --cdrom=/data/iso/cn_windows_7_professional_with_sp1_x64_dvd_u_677031.iso \
    --os-variant=windows \
    --disk /data/drivers/virtio-win-0.1.126_amd64.vfd,device=floppy  \
    --network bridge=virbr0,model=virtio \
    --noautoconsole \
    --accelerate \
    --hvm --virt-type kvm\
    --graphics vnc,listen=0.0.0.0,port=5907 \
    --cpu host-passthrough \
    --force --autostart


virt-install \
    --name win10 \
    --memory 2048 \
    --vcpus sockets=1,cores=1,threads=2 \
    --disk /data/vms/win10.qcow2,bus=virtio,format=qcow2,size=100 \
    --cdrom=/data/iso/win10.iso \
    --os-variant=windows \
    --disk /data/drivers/virtio-win-0.1.185_amd64.vfd,device=floppy  \
    --network bridge=virbr0,model=virtio \
    --noautoconsole \
    --accelerate \
    --hvm --virt-type kvm\
    --graphics vnc,listen=0.0.0.0,port=5910 \
    --cpu host-passthrough 

# Install Centos7 

virt-install \
--name=centos7-base \
--vcpus=1 \
--memory=2048 \
--os-type=linux --os-variant=rhel7  \
--location=/data/iso/CentOS-7-x86_64-Minimal-2003.iso \
--disk path=/data/vms/centos7-base.qcow2,size=30,format=qcow2 \
--network bridge=virbr0 \
--graphics none \
--extra-args='console=ttyS0,target_type=serial' \
--force
```


## 快照功能

```

# 创建快照
virsh snapshot-create win10
# 查看当前快照
virsh snapshot-current  win10

## NOTE
# 使用virsh或者xml启动时候，一定确保你的qcow2所在的行在最前面。
1.kvm克隆
kvm 虚拟机有两部分组成：img镜像文件和xml配置文件（/etc/libvirt/qemu
克隆命令：virt-clone -o rhel6- 71 -n xuegod63-kvm2 -f /var/lib/libvirt/images/xuegod63-kvm2.img

virt-clone -o 原虚拟机 -n 新虚拟机 -f 新img文件
对比配置文件，将两份xml文件做diff对比，里面只修改了name、img、Mac 3个位置信息
克隆完成后，需要修改新虚拟机的网卡配置，并删除/etc/udev/rule.d/70-*-net文件，

2.快照(snapshot)
kvm默认格式为raw格式，如需要修改镜像文件格式。需要配置xml文件
查看镜像文件格式qemu-ig info 镜像文件

1）、转换格式（将raw格式转换为qcow2格式）
qemu-img convert -f raw -O qrow2 /var/lib/libvert/images/xuegod63-kvm2.img
需要修改xml文件virsh edit 虚拟机

2）、创建快照
qemu-img snapshot-create 虚拟机（可以用snapshot-create-as指定快照名称）

3）、快照管理
qemu-img snapshot-list

4）、恢复快照

查看虚拟机状态：virsh domstate xuegod63-kvm2
恢复快照：virsh snapshot-revert 虚拟机 快照名
查看当前快照: virsh snapshot-current xuegod63-kvm2
快照目录：/var/lib/libvert/qemu/snapshot/虚拟机
删除快照： virsh snapshot-delete 虚拟机 快照名
```

# FAQ

## 1. qemu-system 管理不到 虚拟机

> 修改对应的配置。
```xml

<channel type='unix'>
    <source mode='bind'/>
    <target type='virtio' name='org.qemu.guest_agent.0'/>
    <address type='virtio-serial' controller='0' bus='0' port='1'/>
</channel>
```

## 2. 从现有虚拟机启动

```bash

virt-install \
    -n debian10 -r 4096 \
    --graphics vnc,listen=0.0.0.0,port=6021  \
    --vcpus 4 --disk \
    path=/home/superman/vms/debian10.qcow2,size=60,format=qcow2,bus=virtio,target=vda \
    --network bridge=br0,model=virtio --os-type linux --os-variant debian9 \
    --boot hd

virt-install \
    -n cent-base -r 4096 \
    --graphics vnc,listen=0.0.0.0,port=6027  \
    --vcpus 4 --disk \
    path=/home/superman/vms/centos7-base.qcow2,size=60,format=qcow2,bus=virtio,target=vda \
    --network bridge=br0,model=virtio --os-type linux --os-variant rhel7   \
    --boot hd

virt-install \
    --name=oeuler2003 \
    --vcpus=2 \
    --memory=4096 \
    --os-type=linux --os-variant=rhel8.0  \
    --cdrom /home/superman/iso/openEuler-20.03-LTS-TEST.iso \
    --disk path=/home/superman/vms/openeuler2003-test.qcow2,size=40,format=qcow2 \
    --network bridge=br0 \
    --network bridge=virbr0 \
    --vnc --vncport=6630 --vnclisten=0.0.0.0     \
    --force

    --graphics none \
    --extra-args='console=tty0 console=ttyS0,115200n8 serial' \

virt-install   \
    --name=ubuntu2004-base  \
    --vcpus=2   \
    --memory=4096   \
    --os-type=linux \
    --os-variant=ubuntu18.04   \
    --cdrom /home/superman/iso/ubuntu-20.04.2-live-server-amd64.iso   \
    --disk path=/home/superman/vms/ubuntu2004.qcow2,size=20,format=qcow2 \
    --network bridge=br0    \
    --network bridge=virbr0     \
    --vnc --vncport=5920 --vnclisten=0.0.0.0     \
    --force

```